#!/bin/bash

#######################################################################
#           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE               #
#                    Version 2, December 2004                         #
#                                                                     #
# Copyright (C) 2011-12 Sampo Savolainen and Jari Suominen            #
#                                                                     #
# Everyone is permitted to copy and distribute verbatim or modified   #
# copies of this license document, and changing it is allowed as long #
# as the name is changed.                                             #
#                                                                     #  
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE              #
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION   #
#                                                                     #
#  0. You just DO WHAT THE FUCK YOU WANT TO.                          #
#######################################################################

# COMMANDS = array of i commands
# REALCMDS = array of executable locations for each command
# NAMES = array of command lines starting each command
# PIDS = pid for each (i) command
# N = will contain number of apps to run
# I = the app index in the check loop
# D = delay between polling

# Checking arguments:
# $1 - (required) Should be link to the file with list of commands
# $2 - (optional) length of delay between PID scans in seconds (float)

INTERVAL=1
VERBOSE=0
QUIET=0

for i in $*
do
	case $i in
    -i=*|--interval=*)
    INTERVAL=`echo $i | sed 's/[-a-zA-Z0-9]*=//'`
    if ! [[ "$INTERVAL" =~ ^[0-9]+([.][0-9]+)?$ ]] ; then
      exec >&2; echo "#################################### $INTERVAL is not a number: exiting."; exit 1
    fi
    ;;
    -v*|--verbose*)
    VERBOSE=1
    echo "#################################### verbose mode on"
    ;;
    -q*|--quiet*)
    QUIET=1
    echo "#################################### quiet mode does not function yet"
    ;;
    -h*|--help*)
    echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    echo 'runem.sh: A naive bash script for running a set of programs and making sure they keep on running'
    echo '          Copyright (C) 2011-2012 Sampo Savolainen and Jari Suominen'
    echo ''
    echo 'File given as parameter should be a text file with one command to be executed on each line.'
    echo ''
    echo 'Usage: runem.sh [file] [-options] [-i seconds]'
    echo ''
    echo '  -q,--quiet    Do not produce any output.       '
    echo '  -v,--verbose  Show the output(s) of started commands.'
    echo '  -i,--interval Specify a value in ms that should be used as polling interval (default 1.0 sec)' 
    echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    exit 0
    ;;
    *)
    #echo 'AAAAAAAAAAAAA'
    # unknown option
    ;;
  	esac
    #echo '!' $i '?'
done

D=1
if [ $# -lt 1 ] ; then
  echo "#################################### no command list provided: exiting."
  exit 1
else
	cat $1 &> /dev/null
	if [ $? -eq 1 ] ; then
		echo "#################################### file: '$1' don't exist: exiting."
		exit 1
	fi
fi

N=0
while read CMD; do
  if [ "$CMD" != "" ]; then
    echo '#################################### command '$N': '$CMD
    COMMANDS[$N]="$CMD"
    REALCMDS[$N]=$(which ${COMMANDS[$N]/ */})
    N=$[$N+1]
  fi
done < <(cat $1); ##(cmd.list);

echo "#################################### will scan commands every $INTERVAL seconds."

N=$[$N-1]

## A slightly naive function to kill all started sub-processes.
##
## If processes are left behind, make the function loop over the PID list
## until each PID has vanished (or is associated with an executable that
## is not the executable for said command.
function kill_em_all()
{
  echo ''
  echo '#################################### Killing all started programs'
  for I in $(seq 0 $N); do
    if [ ! /proc/${PIDS[$I]}/exe -ef ${REALCMDS[$I]} ]; then
      #echo '###PID dissappeared' $I '!'
      if [ -f /proc/${PIDS[$I]}/stat ]; then
        #echo '### PID existed!'
        if [ `cat /proc/${PIDS[$I]}/stat | awk '{print $2}'` = ${NAMES[$I]} ]; then       
          echo '#################################### '${REALCMDS[$I]}': killing'
          kill ${PIDS[$I]}
        else
          echo '#################################### '${REALCMDS[$I]}': was already stopped'
        fi
      else
        echo '#################################### '${REALCMDS[$I]}': was already stopped'
      fi
    else
      echo '#################################### '${REALCMDS[$I]}': killing'
      kill ${PIDS[$I]}
    fi
  done

  exit
}

## Make sure we kill sub-processes if/when the script is terminated
trap kill_em_all INT TERM

## Main loop:
##
## For each command
##   - do we have a pid for it? if no => start command
##   - is the /proc/$PID/exe entry same as for the command? if no => start command
##   - to start command: Run command as a background process, store the pid
##     and wait for 1 second before continuing
## Wait for one second
## Restart loop

while [ 1 ]; do

  for I in $(seq 0 $N); do

    START=false

    if [ "${PIDS[$I]}" == "" ]; then
      echo '#################################### '${REALCMDS[$I]}': starting'
      START=true

    elif [ ! /proc/${PIDS[$I]}/exe -ef ${REALCMDS[$I]} ]; then
      #echo '###PID dissappeared'
      if [ -f /proc/${PIDS[$I]}/stat ]; then
        if [ ! `cat /proc/${PIDS[$I]}/stat | awk '{print $2}'` = ${NAMES[$I]} ]; then       
          #echo '###file exists but is weird !'
          if [ ! `cat /proc/${PIDS[$I]}/stat | awk '{print $2}'` = '(runem.sh)'} ]; then
            #echo 'oh my GOD!!!!'
            NAMES[$I]=`cat /proc/${PIDS[$I]}/stat | awk '{print $2}'`
          else
            echo '#################################### '${REALCMDS[$I]}':' ${PIDS[$I]} ${NAMES[$I]} 'has stopped, restarting'
            START=true
          fi
        fi
      else
        #echo '###file dont exist !'
        echo '#################################### '${REALCMDS[$I]}':' ${PIDS[$I]} ${NAMES[$I]} 'has stopped, restarting'
        START=true
      fi
    fi
    #echo 'almost' $N ' ' $I '!'
    if [ $START = "true" ]; then
      if [ $VERBOSE = 1 ]; then
        ${COMMANDS[$I]} &
      else
        ${COMMANDS[$I]} > /dev/null &
      fi
      ## store pid
      PIDS[$I]=$!
      NAMES[$I]=`cat /proc/${PIDS[$I]}/stat | awk '{print $2}'`
      echo '#################################### '${REALCMDS[$I]}':' ${PIDS[$I]} ${NAMES[$I]} 'started'
      sleep 1s
    fi
  done

  sleep $INTERVAL
done
