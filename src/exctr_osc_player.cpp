/*
	This file is part of exctr.

	Copyright (C) 2011  Jari Suominen

    ttymidi is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ttymidi is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ttymidi.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <argp.h>
#include <fcntl.h>
#include <signal.h>
#include <pthread.h>
#include <iostream>
#include <fstream>
#include <cstring>

#include <lo/lo.h>

#include <linux/serial.h>
#include <linux/ioctl.h>
#include <asm/ioctls.h>
#include <termios.h>

#define OUTPUT_BUFFER_SIZE 1024

#define MAX_DEV_STR_LEN               32
#define MAX_MSG_SIZE                1024

#include "boost/date_time/posix_time/posix_time.hpp"
namespace pt = boost::posix_time;
using namespace std;

/* change this definition for the correct port */
//#define _POSIX_SOURCE 1 /* POSIX compliant source */

bool run;

/* --------------------------------------------------------------------- */
// Program options

static struct argp_option options[] = 
{
	{"ip" , 'i', "IP" , 0, "IP address (or URL, for example foo.dyndns.org) where OSC messages are sent. Default = 127.0.0.1" },
	{"sport"     , 'p', "SEND_PORT", 0, "Port where OSC messages are sent. Default = 7000" },
	{"rport"     , 'r', "RECEIVING_PORT", 0, "Port for incoming OSC traffic. Default is port defined with -p" },
	{"file" , 'f', "NAME" , 0, "Name of Alsa MIDI client created. Default = oscmidi" },
	{"verbose"      , 'v', 0     , 0, "For debugging: Produce verbose output" },
  {"tcp"      , 't', 0     , 0, "Will use TCP instead of UDP" },
  {"server"      , 's', 0     , 0, "Will run in server mode" },
	{ 0 }
};

typedef struct _arguments
{
	int verbose;
  int tcp, server;
	char ip[MAX_DEV_STR_LEN];
	char file[MAX_DEV_STR_LEN];
	char port[MAX_DEV_STR_LEN];
	char rport[MAX_DEV_STR_LEN];
} arguments_t;


static error_t parse_opt (int key, char *arg, struct argp_state *state)
{
	/* Get the input argument from argp_parse, which we
	   know is a pointer to our arguments structure. */
	arguments_t *arguments = (arguments_t*)(state->input);
	
	switch (key)
	{
		case 'v':
			arguments->verbose = 1;
			break;
    case 't':
			arguments->tcp = 1;
			break;
    case 's':
			arguments->server = 1;
			break;
		case 'i':
			if (arg == NULL) break;
			strncpy(arguments->ip, arg, MAX_DEV_STR_LEN);
			break;
		case 'f':
			if (arg == NULL) break;
			strncpy(arguments->file, arg, MAX_DEV_STR_LEN);
			break;
		case 'p':
			if (arg == NULL) break;
            strncpy(arguments->port, arg, MAX_DEV_STR_LEN);
			break;
		case 'r':
			if (arg == NULL) break;
			 strncpy(arguments->rport, arg, MAX_DEV_STR_LEN);
			break;
		case ARGP_KEY_ARG:
		case ARGP_KEY_END:
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

void arg_set_defaults(arguments_t *arguments)
{
  char *ip_temp = (char *)"127.0.0.1";
  char *port_temp = (char *)"7000";
  char *rport_temp = (char *)"";
	strncpy(arguments->ip, ip_temp, MAX_DEV_STR_LEN);
  strncpy(arguments->port, port_temp, MAX_DEV_STR_LEN);
  strncpy(arguments->rport, rport_temp, MAX_DEV_STR_LEN);  
	arguments->verbose      = 0;
  arguments->tcp      = 0;
  arguments->server      = 0;
	char *file_temp = (char *)"";
	strncpy(arguments->file, file_temp, MAX_DEV_STR_LEN);	
}

const char *argp_program_version     = "exctr_osc_player x666x";
const char *argp_program_bug_address = "satan@heaven.mil";
static char doc[]       = "exctr_osc_server";
static struct argp argp = { options, parse_opt, 0, doc };
arguments_t arguments;


void exit_cli(int sig)
{
	run = false;
}

void error(int num, const char *msg, const char *path)
{
    printf("liblo server error %d in path %s: %s\n", num, path, msg);
    fflush(stdout);
}

int process_incoming_osc(const char *path, const char *types, lo_arg ** argv,
                    int argc, void *data, void *user_data) {
  try {

    
                
    if( std::strcmp( path, "/exctr/frequency" ) == 0 && argc == 2) {
      char bytes[] = {0xF0, argv[0]->i, (argv[1]->i) & 0x7F, (argv[1]->i) >> 7}; 
      if (arguments.verbose)
        cout << "\t/exctr/frequecy node=" << argv[0]->i << " frequency=" << argv[1]->i << endl;             
    }
    		else {
      if (arguments.verbose)
			  std::cout << "\tOSC received '" << path << "' message, which is not exctr-message!" << endl;
        return 1;
	  }
       
  } catch ( ... ) {
    if (arguments.verbose)
      std::cout << "error while parsing message: " <<  "\n";
  } 
  //tcflush(serial, TCIFLUSH); // ?
  return 1;
}

/* --------------------------------------------------------------------- */
// Main program

main(int argc, char** argv)
{
  struct termios oldtio, newtio;
	/* 
	 * read command line arguments
	 */
  arg_set_defaults(&arguments);
	argp_parse(&argp, argc, argv, 0, 0, &arguments);

  if (arguments.verbose)
    cout << endl << "Exctr OSC Player Copyright (C) 2014  Jari Suominen" << endl << "This program comes with ABSOLUTELY NO WARRANTY."
      << endl << "This program is free software: you can redistribute it and/or modify" << endl
      << "it under the terms of the GNU General Public License as published by" << endl
      << "the Free Software Foundation, either version 3 of the License, or" << endl
      << "(at your option) any later version." << endl << endl;
	
  /*
   * OSC In
   */
  if (strlen(arguments.rport) == 0) {
    strncpy(arguments.rport, arguments.port, MAX_DEV_STR_LEN);
	}
  lo_server_thread st;
  if (arguments.tcp) {
    st = lo_server_thread_new_with_proto(arguments.rport,LO_TCP, error);
  } else {
    st = lo_server_thread_new_with_proto(arguments.rport,LO_UDP, error);
  }

  /* add method that will match any path and args */
  lo_server_thread_add_method(st, NULL, NULL, process_incoming_osc, NULL);
  lo_server_thread_start(st);
  if (arguments.verbose) {  
    cout << "Waiting for OSC messages at port " << arguments.rport << " using";
    if (arguments.tcp)
      cout << " TCP." << endl;
    else
      cout << " UDP." << endl;
  }

	signal(SIGINT, exit_cli);
	signal(SIGTERM, exit_cli);
	
  //	s->Run();

	/* Here we wait until ctrl-c or kill signal will be sent... */
  run = true;

	lo_address transmit_socket;
	transmit_socket = lo_address_new(arguments.ip, arguments.port);

  ifstream file;
  file.open(arguments.file);
  pt::ptime zero = pt::microsec_clock::local_time();
  
  if (!arguments.server && !file.is_open()) {
    if (arguments.verbose)
      cout << "Can't read file: " << arguments.file << endl;
    exit(1);
  }

  string timestamp, address, types;
  pt::ptime playstarted = pt::microsec_clock::local_time();

  while((file >> timestamp >> address >> types) && run) {
    pt::time_duration td = pt::duration_from_string(timestamp);
    //td = td - (pt::microsec_clock::local_time() - playstarted);
    
    if (td.total_microseconds() > 0)
      usleep(td.total_microseconds());
    
    if (arguments.verbose)
      cout << timestamp << ' ' << address;

    //pt::ptime t = pt::time_from_string(timestamp);
    
    lo_message l = lo_message_new();
  
    for (int i = 0; i < types.length(); i++) {
      if (types[i] == 'i') {
        int arg;
        file >> arg;
        if (arguments.verbose)
          cout << ' ' << arg;
        lo_message_add(l,"i",arg);
      } else if (types[i] == 's') {
        string arg;
        file >> arg;
        if (arguments.verbose)
          cout << ' ' << arg;
        lo_message_add(l,"s",arg.c_str());
      } 
    }
      
    lo_send_message(transmit_socket,address.c_str(),l);

    // Should the message be freed here? 
    lo_message_free(l);

    if (arguments.verbose)
      cout << endl;
  }

  /*while (run && arguments.server) {
  	sleep(1000);
  }*/

	/* Destruction routine */
	if (arguments.verbose)
		cout << "\r   " << endl << "Stopping [OSC]->[Alsa] communication..." << endl;
	void* status;
  lo_server_thread_stop(st);
  lo_server_thread_free(st);
  usleep(1000); // Attempt to make sure that there is time to free the port. But might not help at all in this!
	if (arguments.verbose) 
		cout << "Morjens!" << endl;
	cout << endl;
}

