all:
	g++ -o exctr_osc_player ./src/exctr_osc_player.cpp -llo -pthread -lboost_date_time
clean:
	rm -f exctr_osc_player
install:
	mkdir -p $(DESTDIR)/usr/local/bin
	cp exctr_osc_player $(DESTDIR)/usr/local/bin
uninstall:
	rm -f $(DESTDIR)/usr/local/bin/exctr_osc_player
